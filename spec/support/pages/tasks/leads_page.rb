class LeadsPage < AbstractPage
  include PageObject

  page_url "https://app.futuresimple.com/leads"

  #lists
  unordered_list(:notes_list, :class => "feed-items")

  #buttons
  link(:add_new_lead, :css => "#leads-new")
  button(:save_lead, :css => ".save")
  button(:save_note, :class => "btn btn-inverse hide", :text => "Save")

  #inputs
  text_field(:first_name, :css => "#lead-first-name")
  text_field(:last_name, :css => "#lead-last-name")
  text_field(:company_name, :css => "#lead-company-name")
  text_area(:note_text, :name => "note")

end
