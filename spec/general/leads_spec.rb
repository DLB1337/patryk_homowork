require 'spec_helper'
#test
lorem = Faker::Lorem.sentence

shared_examples "Create lead and check it" do |lorem|

  it "Adds new lead" do
    on(LeadsPage).add_new_lead_element.when_visible(10).click
    on(LeadsPage).first_name_element.when_visible(10).value = Faker::Name.first_name
    on(LeadsPage).last_name_element.when_visible(10).value = Faker::Name.last_name
    on(LeadsPage).company_name_element.when_visible(10).value = Faker::Company.name
    on(LeadsPage).save_lead_element.when_visible(10).click
  end

  it "Adds new note" do
    on(LeadsPage).note_text_element.when_visible(10).value = lorem
    on(LeadsPage).save_note_element.when_visible(10).click
  end

  it "checks task details in edit mode" do
    sleep 5
    expect(@current_page.notes_list).to include lorem
  end
end

describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end
    include_examples "Create lead and check it", lorem, true, false, false
end
